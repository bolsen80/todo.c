# ASM-to-C challenge

https://github.com/tsoding/todo.asm is a todo web app + server in x86 assembler (using FASM). It implements a tiny bit of HTTP to serve a page and allow a POST of todos.

For improving my C and x86 assembly, I started to rewrite it in C. I started this during a lunch and hacking session I have at my company.

So far:
 1. Implement GET of index page
 2. Implement 405 method not allowed

Still need to handle other method types for the application.

I am tryin to be faithful to what is done in todo.asm, but I need to cheat a little to use libc functions correctly instead of direct syscalls, like in todo.asm.

## Build + install

Builds with gcc and make:

`make`

(pedantic means be strict with ISO C.)

Run:

`./todo`
and then go to: http://localhost:14619
