#ifndef SERVE_H
#define SERVE_H

#include <stdbool.h>

#define TODO_FILE_PATH "todo.db"

#define M_GET "GET "
#define M_POST "POST "
#define M_PUT "PUT "
#define M_DELETE "DELETE "

// When doing TODO_SIZE*TODO_CAP, we can have 255 entries at up to 256 chars each.
#define TODO_SIZE 256
#define TODO_CAP 255

#define FATAL_ERROR(result, error) do { if (result < 0) { printf("result is: %d", result); write(STDERR_FILENO, error, strlen(error)); exit(1); } } while(0);

#define INDEX_PAGE_HEADER "<h1>To-Do</h1>" "\n" \
    "<ul>"

#define INDEX_PAGE_FOOTER                                                      \
  "</ul>"                                                                      \
  "\n"                                                                         \
  "<form method='post' action='/' enctype='text/plain'>"                       \
  "\n"                                                                         \
  "<input type='text' name='todo' autofocus>"                                  \
  "\n"                                                                         \
  "<input type='submit' value='add'>"                                          \
  "\n"                                                                         \
  "</form>"                                                                    \
  "\n"

#define INDEX_PAGE_RESPONSE "HTTP/1.1 200 OK\r\n" \
    "Content-Type: text/html; charset=utf8\r\n" \
    "Connection: close\r\n" \
    "\r\n"

#define TODO_HEADER "<li>"
#define TODO_FOOTER "</li>\n"
#define DELETE_BUTTON_PREFIX                                            \
    "<form style='display: inline' method='post' action='/'>"           \
    "<button style='width: 25px' type='submit' name='delete' "          \
    "value='"
#define DELETE_BUTTON_SUFFIX "'>x</button></form>\n"

#define ERROR_400_PAGE "HTTP/1.1 400 Bad Request\r\n"                          \
    "Content-Type: text/html; charset=utf-8\r\n"                        \
    "Connection: close\r\n"                                             \
    "\r\n"                                                              \
    "<h1>Bad Request</h1>\n"

#define ERROR_404_PAGE                                                         \
  "HTTP/1.1 404 Not Found\r\n"                                        \
  "Content-Type: text/html; charset=utf-8\r\n"                                 \
  "Connection:close\r\n\r\n"                                                   \
  "<h1>File note found</h1>\n"

#define ERROR_405_PAGE              \
  "HTTP/1.1 405 Method Not Allowed\r\n" \
  "Content-Type: text/html; charset=utf-8\r\n" \
  "Connection:close\r\n\r\n" \
  "<h1>Method not allowed</h1>\n"

    void
    load_todos();
void save_todos();
int add_todo(char* buffer, int buffer_len);
void delete_todo(int index);
void serve_400(int accept_fd);
void serve_404(int accept_fd);
void serve_405(int accept_fd);
bool starts_with(char *request, int request_len, char *prefix, int prefix_len);
void serve_index_page(int accept_fd);
void handle_get(int accept_fd, char *buffer, int buffer_len);
void handle_post(int accept_fd, char *buffer, int buffer_len);

#endif
