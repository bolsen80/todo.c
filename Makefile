OBJECTS = main.o serve.o
CC = gcc
CFLAGS = -g -Wall -Wextra -Werror -c
BIN = todo

all: $(OBJECTS)
	$(CC) -o todo $(OBJECTS)

%.o: %.c
	$(CC) $(CFLAGS)	$< -o $@

clean:
	rm *.o $(BIN)
