#include <netinet/in.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <errno.h>

#include "serve.h"

const int MAX_CONN = 5;
const int REQUEST_CAP = 128 * 1024;
const char *start = "INFO: Starting web server!\n";
const char *socket_trace_msg = "INFO: Creating a socket ...\n";
const char *error = "FATAL ERROR!\n";
const char *bind_char_msg = "INFO: Binding the socket...\n";
const char *listen_trace_msg = "INFO: Listening to the socket...\n";
const char *accept_trace_msg = "INFO: Waiting for client connections...\n";

void next_request(int socket_fd) {
    struct sockaddr_storage their_addr;
    char buffer[REQUEST_CAP];
    // we seem to need to clear the buffer ...
    for (int i = 0; i < REQUEST_CAP; i++) buffer[i] = '\0';

    socklen_t addr_size;

    write(STDERR_FILENO, accept_trace_msg, strlen(accept_trace_msg));
    addr_size = sizeof their_addr;

    int accept_fd = accept(socket_fd, (struct sockaddr *)&their_addr, &addr_size);

    int request_size = read(accept_fd, buffer, REQUEST_CAP);
    FATAL_ERROR(request_size, error);

    if (starts_with(buffer, REQUEST_CAP, M_GET, 4)) {
        // in original:
        // add [request_cur], post_len
        // sub [request_len], post_len
        //
        // We are positioning the pointers to the correct points in the buffer.
        //
        // Effectively, the buffer points at / (in correct cases.)
        // GET /
        //     ^
        // We are also consuming 4 less bytes of input, so we need to read 4 bytes less.
        handle_get(accept_fd, buffer + 4, REQUEST_CAP - 4);
    } else if (starts_with(buffer, REQUEST_CAP, M_POST, 5)) {
        handle_post(accept_fd, buffer + 5, REQUEST_CAP - 5);
        /*   else if (starts_with(buffer, REQUEST_CAP, M_PUT, 3)) { */
      /*   handle_put(buffer + 4); */
      /* } else if (starts_with(buffer, REQUEST_CAP, M_DELETE, 6)) { */
      /*   handle_delete(buffer + 7); */
    } else {
      serve_405(accept_fd);
    }

    close(accept_fd);
}

int main() {

    load_todos();

    int result;
    int yes = 1;

    write(STDOUT_FILENO, start, strlen(start));
    write(STDOUT_FILENO, socket_trace_msg, strlen(socket_trace_msg));
    int sfd = socket(AF_INET, SOCK_STREAM, 0);
    FATAL_ERROR(sfd, error);

    result = setsockopt(sfd, SOL_SOCKET, SO_REUSEADDR, &yes, 4);
    FATAL_ERROR(result, error);

    result = setsockopt(sfd, SOL_SOCKET, SO_REUSEPORT, &yes, 4);
    FATAL_ERROR(result, error);

    write(STDOUT_FILENO, bind_char_msg, strlen(bind_char_msg));

    /* struct sockaddr_in sockinfo; */
    /* sockinfo.sin_family = AF_INET; */
    /* sockinfo.sin_port = 14619; */
    /* sockinfo.sin_addr.s_addr = INADDR_ANY; */

    struct addrinfo hints;
    struct addrinfo *servinfo;
    memset(&hints, 0, sizeof(hints)); // zero out memory
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;

    result = getaddrinfo(NULL, "6969", &hints, &servinfo);
    FATAL_ERROR(result, error);

    result = bind(sfd, servinfo->ai_addr, servinfo->ai_addrlen);

    FATAL_ERROR(result, error);

    write(STDERR_FILENO, listen_trace_msg, strlen(listen_trace_msg));
    result = listen(sfd, MAX_CONN);
    FATAL_ERROR(result, error);

    while (true) {
      next_request(sfd);
    }

    return 0;
}
