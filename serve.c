#include "serve.h"
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <stdlib.h>

/**
 * In tsoding/todo.asm, it uses straight write() syscalls instead of the
 high-level send(2).
 * In the man page:
 * "The only difference between send()  and  write(2)  is  the  presence  of
       flags. With a zero flags argument, send() is equivalent to write(2)."
 */

const char *index_route = "/ ";
const int index_route_len = 2;

// why here? to avoid the c compiler putting this into stack memory.
// yeah yeah, "no globals" ...
char todos_buffer[TODO_CAP * TODO_SIZE];
int todo_end_offset = 0;

void add_new_todo_and_serve_index_page(int accept_fd, char *buffer,
                                       int buffer_len);
void delete_todo_and_serve_index_page(int accept_fd, char* index);


void serve_400(int accept_fd) {
    write(accept_fd, ERROR_400_PAGE, strlen(ERROR_400_PAGE));
}

void serve_404(int accept_fd) {
    write(accept_fd, ERROR_404_PAGE, strlen(ERROR_404_PAGE));
}

void serve_405(int accept_fd) {
    write(accept_fd, ERROR_405_PAGE, strlen(ERROR_405_PAGE));
}

bool starts_with(char *request, int request_len, char *prefix, int prefix_len) {
    char buffer[10];
    strncpy(buffer, request, prefix_len);
    if (strncmp(buffer, prefix, prefix_len) == 0) {
        return true;
    }
    // TODO: do something with request_len for now hahaha
    int noop() { return request_len; } noop(request_len);

    return false;
}

int find_char(char *buffer, size_t buffer_len, char c) {

    size_t pos = 0;
    if (buffer_len == 0) { return -1; }
    while (buffer_len != pos) {
        if (*buffer == c) { return pos; }
        pos++;
        buffer++;
    }

    return pos;
}

/**
 * This iterates through headers to get to the body of a request.
 * It returns an int where the pointer should be.
 *
 * In todo.asm, drop_http_header iterates the pointer on each increment.
 * This is doing it all at once.
 *
 * This is written like rejecting lines of comments in a parsing context.
 */
int drop_http_header(char *buffer, int buffer_len) {
    bool reached_end = false;
    int buffer_pos = 0;
    int final_pos = 0;
    do {
      if (starts_with(buffer + buffer_pos, buffer_len - buffer_pos, "\r\n", 2) > 0) {
        // we found the beginning of the body here (MIME splits header and body
        // with two CRLF pairs.)
        final_pos += 2;
        reached_end = true;
      } else {
          buffer = buffer + buffer_pos;
          buffer_len = buffer_len - buffer_pos;

          int found_char = find_char(buffer, buffer_len, '\n');
          if (found_char == 0) {
              // invalid header: we return 0 for a false header.
              return 0;
          }

          buffer_pos = found_char + 1;
          final_pos += found_char + 1;
      }
    } while (!reached_end);

    return final_pos;
}

void render_todos_as_html(int accept_fd) {
    int todo_count = todo_end_offset / TODO_SIZE;
    for (int i = 0; i < todo_count; i++) {
        write(accept_fd, TODO_HEADER, strlen(TODO_HEADER));
        write(accept_fd, DELETE_BUTTON_PREFIX, strlen(DELETE_BUTTON_PREFIX));
        char s_index[8];
        int s_size = sprintf(s_index, "%d", i);
        write(accept_fd, s_index, s_size);
        write(accept_fd, DELETE_BUTTON_SUFFIX, strlen(DELETE_BUTTON_SUFFIX));

        // actual todo
        char* current_todo = todos_buffer + (i * TODO_SIZE);

        // remember that plain char type is signed so it can overflow
        unsigned char todo_size = current_todo[0];
        write(accept_fd, current_todo+1, todo_size);

        write(accept_fd, TODO_FOOTER, strlen(TODO_FOOTER));
    }
}

void serve_index_page(int accept_fd) {
    write(accept_fd, INDEX_PAGE_RESPONSE, strlen(INDEX_PAGE_RESPONSE));
    write(accept_fd, INDEX_PAGE_HEADER, strlen(INDEX_PAGE_HEADER));
    render_todos_as_html(accept_fd);
    write(accept_fd, INDEX_PAGE_FOOTER, strlen(INDEX_PAGE_FOOTER));
}

void handle_get(int accept_fd, char *buffer, int buffer_len) {
    bool result = starts_with(buffer, buffer_len, (char *)index_route, index_route_len);

    if (result) {
        serve_index_page(accept_fd);
    } else {
        serve_404(accept_fd);
    }
}

void handle_post(int accept_fd, char *buffer, int buffer_len) {

    char *todo_data_prefix = "todo=";
    char *delete_data_prefix = "delete=";

    int route =
        starts_with(buffer, buffer_len, (char *)index_route, index_route_len);
    if (route < 0) {
        serve_404(accept_fd);
    }
    int dropped = drop_http_header(buffer, buffer_len);
    if (dropped == 0) {
        // invalid header because drop_http_header couldn't iterate through the
        // header
        serve_400(accept_fd);
    } else {
        buffer = buffer + dropped;
        buffer_len = buffer_len - dropped;
    }

    int data_prefix =
        starts_with(buffer, buffer_len, "todo=", strlen(todo_data_prefix));
    int end_data =
        find_char(buffer, buffer_len, '\r');
    if (data_prefix > 0) {
        add_new_todo_and_serve_index_page(accept_fd,
                                          buffer + strlen(todo_data_prefix),
                                          end_data - strlen(todo_data_prefix));
      return;
    }

    data_prefix = starts_with(buffer, buffer_len, "delete=", strlen(delete_data_prefix));
    if (data_prefix > 0) {
        delete_todo_and_serve_index_page(accept_fd,
                                         buffer + strlen(delete_data_prefix));
      return;
    }

    // If todo= or delete= is not present, return 400 Bad Request
    serve_400(accept_fd);
}

void load_todos() {
    // Could also use fopen(2)
    int fd = open(TODO_FILE_PATH, O_RDONLY, 0);

    if (fd < 0) {
        close(fd);
    }

    struct stat statbuf;
    int result = fstat(fd, &statbuf);
    if (result < 0) {
        close(fd);
    }

    if ((statbuf.st_size % TODO_SIZE) != 0) {
        close(fd);
    }

    int size = statbuf.st_size;
    int max_size = TODO_CAP * TODO_SIZE;
    if (TODO_CAP * TODO_SIZE < size) {
        size = max_size;
    }


    read(fd, todos_buffer, size);
    todo_end_offset = size;

    close(fd);
}

void save_todos() {
    int fd = open(TODO_FILE_PATH, O_CREAT | O_TRUNC | O_WRONLY, 420);
    if (fd < 0) {
        return;
    }

    write(fd, todos_buffer, todo_end_offset);
    sync();
    close(fd);
}

int add_todo(char *buffer, int buffer_len) {
    if (todo_end_offset >= TODO_SIZE*TODO_CAP) {
        return 1;
    }

    if (buffer_len > 255) {
        buffer_len = 255;
    }

    todos_buffer[todo_end_offset] = (char)buffer_len;
    memcpy(&todos_buffer[todo_end_offset + 1], buffer, buffer_len);
    todo_end_offset += TODO_SIZE;

    return 255;
}

void *todo_pass(char *b, int bl) { return b + bl; }

void delete_todo(int index) {
    int location_to_delete = TODO_SIZE * index;
    if (location_to_delete >= todo_end_offset) {
        return;
    }

    char* start = todos_buffer;
    char* start_to_delete = start + location_to_delete;
    char* end_to_delete = start_to_delete + TODO_SIZE;
    int end_of_delete_index = location_to_delete + TODO_SIZE;

    memcpy(start_to_delete, end_to_delete, todo_end_offset - end_of_delete_index);
    todo_end_offset -= TODO_SIZE;
}

void add_new_todo_and_serve_index_page(int accept_fd, char *buffer, int buffer_len) {
    add_todo(buffer, buffer_len);
    save_todos();
    serve_index_page(accept_fd);
}

void delete_todo_and_serve_index_page(int accept_fd, char* index) {
    int i_index = atoi(index);
    delete_todo(i_index);
    save_todos();
    serve_index_page(accept_fd);
}
